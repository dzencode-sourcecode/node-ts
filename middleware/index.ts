import { handleCors, handleBodyRequestParsing, handleCompression, handleJwt } from './common';

import { handleAPIDocs } from './apiDocs';

export default [handleCors, handleBodyRequestParsing, handleCompression, handleAPIDocs];
export const JwtAuth = [handleJwt];

import http from 'http';
import express from 'express';
import cron from 'node-cron';
import { applyMiddleware, applyRoutes } from './utils';
import middleware, { JwtAuth } from './middleware';
import errorHandlers from './middleware/errorHandlers';
import routes, { routesWithoutAuth } from './services';
import Logger from './lib/logger';
import dotenv from 'dotenv';
import { checkAllTasks } from './controllers/UserTaskStatusController';
dotenv.config();

const logger = new Logger();

const router = express();

applyMiddleware(middleware, router);
applyRoutes(routesWithoutAuth, router);
applyMiddleware(JwtAuth, router);
applyRoutes(routes, router);
applyMiddleware(errorHandlers, router);

cron.schedule('0 1 * * *', () => {
    logger.info('Start job for check all task statuses');
    checkAllTasks();
});

process.on('uncaughtException', e => {
    logger.error('Error uncaughtException: ', e);
    process.exit(1);
});

process.on('unhandledRejection', e => {
    logger.error('Error unhandledRejection: ', e);
    process.exit(1);
});
const { PORT = 65071 } = process.env;
const server = http.createServer(router);

server.listen(PORT, () => logger.info(`Server is running http://localhost:${PORT}`));

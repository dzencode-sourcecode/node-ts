import qs from 'qs';
import { Op } from 'sequelize';
import { getTaskById } from './providers/TasksProvider';
import { updateTaskAggrStatistic } from '../../controllers/UserTaskAggrStatisticController';
import sequelize from '../../database/config/database';
import { UserTask } from '../../database/models/user-task';
import { TaskStatusLog } from '../../database/models/task-status-log';
import { TaskStatisticLog } from '../../database/models/task-statistic-log';
import { UserTaskAggrStatistic } from '../../database/models/user-task-aggr-statistic';
import { UserTaskAggrStatisticStatus } from '../../database/models/user-task-aggr-statistic-status';
import { UserTaskInfo } from '../../database/models/user-task-info';
import Logger from '../../lib/logger';
import moment from 'moment';
import { axiosHooks, HOOK_URLS } from '../../lib/axios';

import dotenv from 'dotenv';

dotenv.config();

const logger = new Logger();

interface BodyTaskInterface {
    taskId: string;
    userCreatorId: string;
    userResponsibleId: string;
    startDate: string;
    endDate: string;
}

/** When dev add task to his dashboard
 * 1. Validate task by id for exist
 * 2. Check for first add task or continue working
 * 3. If first add task then add:
 *      - UserTask
 *      - TaskStatisticLog
 *      - UserTaskAggrStatistic
 *      - updateTaskAggrStatistic
 * 4. Add TaskStatusLog
 */
export const addTask = async (body: BodyTaskInterface): Promise<boolean | undefined> => {
    logger.info(`>>>>>> Come in addTask controller`);
    let result: false | undefined;
    try {
        result = await sequelize.transaction(async t => {
            let newTask = true;

            logger.debug(`run getTaskById with params: taskId: ${body.taskId}`);
            const task = await getTaskById(+body.taskId);
            if (task.data.count === 0) return false;

            logger.debug(
                `run UserTask.findOne with params: 
                taskId: ${body.taskId} | 
                userResponsibleId: ${body.userResponsibleId}`,
            );
            let userTask = await UserTask.findOne({
                where: { taskId: body.taskId, userResponsibleId: body.userResponsibleId },
            });
            if (userTask) {
                newTask = false;
            }
            // default status "3 - WORKING"
            logger.debug(`run UserTaskAggrStatisticStatus.findOne with params: statusId: 3`);
            const taskStatus = await UserTaskAggrStatisticStatus.findOne({ where: { statusId: 3 }, transaction: t });
            console.log('task status', taskStatus);
            if (!taskStatus) throw new Error('Task status not found');
            // if user already was worked with task, then we change only statistics and status
            if (newTask) {
                logger.debug(
                    `run UserTask.create with params: 
                    taskId: ${body.taskId} | 
                    userCreatorId: ${body.userCreatorId} | 
                    userResponsibleId: ${body.userResponsibleId},
                    groupId: ${task.data.results[0].group_id}`,
                );
                userTask = await UserTask.create(
                    {
                        taskId: body.taskId,
                        userCreatorId: body.userCreatorId,
                        userResponsibleId: body.userResponsibleId,
                        groupId: task.data.results[0].group_id,
                    },
                    { transaction: t },
                );

                logger.debug(
                    `run UserTaskInfo.create with params: 
                    taskId: ${body.taskId} | 
                    userResponsibleId: ${body.userResponsibleId} |
                    userCreatorId: ${body.userCreatorId} |
                    userTaskId: ${userTask.id} |
                    deadline: ${task.data.results[0].deadline} |
                    timeEstimate: ${task.data.results[0].time_estimate}`,
                );
                await UserTaskInfo.create(
                    {
                        taskId: body.taskId,
                        userResponsibleId: body.userResponsibleId,
                        userCreatorId: body.userCreatorId,
                        userTaskId: userTask.id,
                        deadline: task.data.results[0].deadline,
                        startDate: body.startDate,
                        endDate: body.endDate,
                        timeEstimate: task.data.results[0].time_estimate,
                    },
                    { transaction: t },
                );

                logger.debug(
                    `run TaskStatisticLog.create with params: 
                    userTaskId: ${userTask.id} | 
                    userId: ${body.userResponsibleId} | 
                    taskId: ${body.taskId} | 
                    value: 0 | 
                    symbol: PLUS`,
                );

                await TaskStatisticLog.create(
                    {
                        userTaskId: userTask.id,
                        userId: body.userResponsibleId,
                        taskId: body.taskId,
                        value: 0,
                        symbol: 'PLUS',
                    },
                    { transaction: t },
                );
                // Update statistic in other module!
                logger.debug(
                    `run UserTaskAggrStatistic.create with params: 
                    userTaskId: ${userTask.id} | 
                    status: ${taskStatus.id} | 
                    taskId: ${body.taskId} | 
                    value: 0 | 
                    symbol: PLUS`,
                );
                await UserTaskAggrStatistic.create(
                    {
                        userTaskId: userTask.id,
                        status: taskStatus.id,
                        staticValue: 0,
                        staticSymbol: 'PLUS',
                        startDate: body.startDate,
                        endDate: body.endDate,
                    },
                    { transaction: t },
                );
            }
            if (userTask) {
                logger.debug(
                    `run TaskStatusLog.create with params: 
                    userTaskId: ${userTask.id} | 
                    userId: ${body.userResponsibleId} | 
                    taskId: ${body.taskId} | 
                    status: ${taskStatus.id}`,
                );
                if (moment().isSame(moment(body.startDate), 'day')) {
                    await TaskStatusLog.create(
                        {
                            userTaskId: userTask.id,
                            userId: body.userResponsibleId,
                            taskId: body.taskId,
                            status: taskStatus.id,
                        },
                        { transaction: t },
                    );
                }
                logger.debug(`run updateTaskAggrStatistic with params: userTaskId: ${userTask.id}`);
                updateTaskAggrStatistic(+userTask.id);
            }
            const axios = await axiosHooks(+body.userResponsibleId);
            const taskComment = `Задача была принята в работу. Дата старта: ${body.startDate} - Дата завершения: ${body.endDate}.`;
            const params = {
                TASKID: body.taskId,
                FIELDS: { POST_MESSAGE: taskComment },
            };
            await axios.get(HOOK_URLS.taskCommentAdd, {
                params,
                paramsSerializer: params => {
                    return qs.stringify(params);
                },
            });
        });

        logger.info(`<<<<<< Come out addTask controller`);
    } catch (error) {
        logger.warn(error);
        logger.info(`<<<<<< Come out catch addTask controller`);

        result = false;
    }

    return result;
};


/**
 * Get all tasks by user ID for range
 */
export const getTasks = async (userResponsibleId: number, startDate: string, endDate: string): Promise<UserTask[]> => {
    logger.debug(
        `run getTasks with params: userResponsibleId: ${userResponsibleId} | startDate: ${startDate} | endDate: ${endDate}`,
    );
    let config = {};
    if (startDate && endDate) {
        config = { [Op.between]: [startDate, endDate] };
    } else if (startDate) {
        config = { [Op.gte]: startDate };
    } else {
        config = { [Op.lte]: endDate };
    }

    return UserTask.findAll({
        where: { userResponsibleId: userResponsibleId, createdAt: config },
        include: [{ all: true }],
        order: ['createdAt'],
    });
};

/**
 * Get all tasks statuses by user and task ID`s
 */
export const getTaskStatusesById = async (userResponsibleId: number, tasks: []): Promise<UserTask[]> => {
    logger.debug(`run getTaskStatusesById with params: userResponsibleId: ${userResponsibleId}`);

    return UserTask.findAll({
        where: { taskId: { [Op.in]: tasks } },
        include: [{ all: true }],
    });
};

import { AxiosResponse } from 'axios';
import dotenv from 'dotenv';
import { API_URLS, axiosWrapper } from '../../../lib/axios';
import Logger from '../../../lib/logger';

dotenv.config();

const logger = new Logger();

export const getTaskById = async (id: number): Promise<AxiosResponse> => {
    logger.info(`>>>>>> Come in getTaskById`);
    logger.debug(`axios get with url: ${API_URLS.tasks}/?tasks=${id}`);
    const response = await axiosWrapper.get(API_URLS.tasks, { params: { tasks: id } });
    logger.debug(response.data);
    logger.info(`<<<<<< Come out getTaskById`);
    return response;
};

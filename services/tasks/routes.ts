import { Request, Response } from 'express';
import {
    validateBodyTask,
    validateRoleManager,
} from '../../middleware/checks';
import {
    addTask,
    removeTask,
    completeTask,
} from './TaskController';

export default [
    {
        path: '/api/v1/tasks',
        method: 'post',
        handler: [
            validateBodyTask,
            async ({ body }: Request, res: Response): Promise<void> => {
                const result = await addTask(body);
                if (result === undefined) {
                    res.status(200).send('Task was added');
                } else {
                    res.status(400).send('Task was not added');
                }
            },
        ],
    },
    {
        path: '/api/v1/tasks/cancel',
        method: 'post',
        handler: [
            validateRoleManager,
            async ({ body }: Request, res: Response): Promise<void> => {
                const result = await removeTask(
                    +body.taskId,
                    +body.userCreatorId,
                    +body.userResponsibleId,
                    +body.reasonId,
                    body.comment,
                );

                if (result === undefined) {
                    res.status(200).send('Task was canceled');
                } else {
                    res.status(400).send('Task was not canceled');
                }
            },
        ],
    },
    {
        path: '/api/v1/tasks/complete',
        method: 'post',
        handler: [
            async ({ body }: Request, res: Response): Promise<void> => {
                const result = await completeTask(+body.taskId, +body.userCreatorId, +body.userResponsibleId);

                if (result === undefined) {
                    res.status(200).send('Task was completed');
                } else {
                    res.status(400).send('Task was not completed');
                }
            },
        ],
    },
];

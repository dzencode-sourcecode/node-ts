import hooksRoutes from './hooks/routes';

import userConfig from './user-config/routes';
import userNotification from './user-notification/routes';

export default [
    ...userConfig,
    ...userNotification,
];

export const routesWithoutAuth = [...hooksRoutes];

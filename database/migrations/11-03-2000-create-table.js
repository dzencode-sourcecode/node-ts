'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('UserTask', {
            id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            userResponsibleId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            userCreatorId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            taskId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            groupId: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('UserTask');
    },
};

module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.bulkInsert(
            'UserTaskAggrStatisticStatus',
            [
                {
                    statusId: 1,
                    status: 'EXPIRED',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    statusId: 2,
                    status: 'APPROACH',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    statusId: 3,
                    status: 'WORKING',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    statusId: 4,
                    status: 'MANAGER_CANCEL',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    statusId: 5,
                    status: 'COMPLETE',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    statusId: 6,
                    status: 'COMPLETE_APPROVE',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],

            {},
        ),

    down: (queryInterface, Sequelize) => queryInterface.bulkDelete('UserTaskAggrStatisticStatus', null, {}),
};

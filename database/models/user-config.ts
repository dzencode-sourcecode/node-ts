import { Model, DataTypes } from 'sequelize';
import sequelize from '../config/database';
import { User } from './user';

/**
 * UserConfig contains information about config users in department pm
 * ```js
 id!: number;
 userId: number;
 webHookLink: string;
 webHookToken: string;
 isValidateToken: boolean;

 // timestamps!
 createdAt!: Date;
 updatedAt!: Date;
 * ```
 */
export class UserConfig extends Model {
    public id!: number;
    public userId!: number;
    public webHookLink!: string;
    public webHookToken!: string;
    public isValidateToken!: boolean;

    // timestamps!
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

UserConfig.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        webHookLink: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        webHookToken: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        isValidateToken: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
    },
    {
        sequelize,
        tableName: 'UserConfig',
    },
);

UserConfig.hasMany(User, { foreignKey: 'userConfigId' });
